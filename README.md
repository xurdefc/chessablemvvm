# README #

This project is a simply html5/css3/javascript developement needed to set up a chess position on a chess board related to a fen notation string which defines the entire chess position. It enables the checking of fen notation syntax and position consistency, as well as the setting of other chess related features:
	- next color piece move
	- white short and long castling rights
	- black short and long castiling rights
	- initial chess games position reverting
	- empty chess board
	- last valid chess postion reverting
	
Additionally, the user has direct access to potencial syntax and/or consistency errors via popover detail messaging.

### What is this repository for? ###

This repository holds the files needed to implement a coding task required by Chessable using a MVVM design pattern and knockout.js MVVM framework

Version: 1

### How do I get set up? ###

The coding project does not required any specific setup. All files needed are either physically present, or loaded via https://cdnjs.cloudflare.com/

No specific configurationrequirement is needed.

This project uses the following external libraries:
	- jquery-3.2.1.js
	- jquery.webui-popover.js
	- chessboard-0.3.0.js
	- chess.js
	- knockout.js

and style sheets:
	- chessboard-0.3.0.css
	- font-awesome.min.css
	- jquery.webui-popover.css

No databases have been used in the project and no specific unit tests files have been implemented.

In order to download and run the project, simply copy all files and directories to specfic directory, and execute the ChessBoardSetup.html file.

### Contribution guidelines ###

None

### Who do I talk to? ###

This project has been developed entirely by Jorge Fernández Canal (xurdef@gmail.com).