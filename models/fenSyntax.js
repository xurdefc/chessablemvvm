// Fen syntax validation based on chess.js
function validate_fenSyntax(fen) {
  var err = [];
  var error_msgs = [
    '',
    'FEN string does not contain six space-delimited fields.',
    'Move number (6th field ***) is not a positive integer.',
    'Half move counter (5th field ***) is not a non-negative integer.',
    'En-passant square (4th field ***) is not valid.',
    'castling availability (3rd field ***) is not of valid format.',
    'Side to move (2nd field  ***) is not of valid format.',
    'Piece positions (***) are not contained in 8 \'/\' delimited rows.',
    'Piece positions (subfield ***) holds consecutive numbers.',
    'Piece positions (subfield ***) does not have valid pieces.',
    'Piece positions (subfield ***) is not of valid length [too short/long].',
    'Illegal en-passant square.',
  ];

  // 1st criterion: 6 space-seperated fields?
  var tokens = fen.split(/\s+/);
  if (tokens.length !== 6) {
    err.push({error: error_msgs[1]});
    console.log(err);
    return err;
  }

  // 2nd criterion: move number field is a integer value > 0?
  if (isNaN(tokens[5]) || (parseInt(tokens[5], 10) <= 0)) {
    err.push({error: error_msgs[2].replace('***', '<mark>\xa0' + tokens[5] + '\xa0</mark>')});
  }

  // 3rd criterion: half move counter is an integer >= 0?
  if (isNaN(tokens[4]) || (parseInt(tokens[4], 10) < 0)) {
    err.push({error: error_msgs[3].replace('***', '<mark>\xa0' + tokens[4] + '\xa0</mark>')});      
  }

  // 4th criterion: 4th field is a valid e.p.-string?
  if (!/^(-|[abcdefgh][36])$/.test(tokens[3])) {
    err.push({error: error_msgs[4].replace('***', '<mark>\xa0' + tokens[3] + '\xa0</mark>')}); 
  }

  // 5th criterion: 3th field is a valid castle-string?
  if( !/^(KQ?k?q?|Qk?q?|kq?|q|-)$/.test(tokens[2])) {
    err.push({error: error_msgs[5].replace('***', '<mark>\xa0' + tokens[2] + '\xa0</mark>')}); 
  }

  // 6th criterion: 2nd field is "w" (white) or "b" (black)?
  if (!/^(w|b)$/.test(tokens[1])) {
    err.push({error: error_msgs[6].replace('***', '<mark>\xa0' + tokens[1] + '\xa0</mark>')}); 
  }

  // 7th criterion: 1st field contains 8 rows?
  var rows = tokens[0].split('/');
  if (rows.length !== 8) {
    err.push({error: error_msgs[7].replace('***', '<mark>\xa0' + tokens[0] + '\xa0</mark>')});       
    return err;
  }

  // 8th criterion: every row is valid?
  for (var i = 0; i < rows.length; i++) {           // check for right sum of fields AND not two numbers in succession   
    var sum_fields = 0;
    var previous_was_number = false;

    for (var k = 0; k < rows[i].length; k++) {
      if (!isNaN(rows[i][k])) {
        if (previous_was_number) {
          err.push({error: error_msgs[8].replace('***', '#'+(i+1).toString()+' <mark>\xa0' + rows[i] + '\xa0</mark>')});
        }
        sum_fields += parseInt(rows[i][k], 10);
        previous_was_number = true;
      } else {
        if (!/^[prnbqkPRNBQK]$/.test(rows[i][k])) {
          err.push({error: error_msgs[9].replace('***', '#'+(i+1).toString()+' <mark>\xa0' + rows[i] + '\xa0</mark>')});
        }
        sum_fields += 1;
        previous_was_number = false;
      }
    }
    if (sum_fields !== 8) {
      err.push({error: error_msgs[10].replace('***', '#'+(i+1).toString()+' <mark>\xa0' + rows[i] + '\xa0</mark>')});
    }
  }

  // 9th criterion: en-passant square and next move comparison
  if ((tokens[3][1] == '3' && tokens[1] == 'w') ||
      (tokens[3][1] == '6' && tokens[1] == 'b')) {
        err.push({error: error_msgs[11]});
  }

  return err;
}