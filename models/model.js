function chessmodel () {
    
    this.fen = '';                            // Fen String
    this.origin = '';                         // Origin of fen string ('user', 'board')
    this.wLongCastle = false;                 // White Long Castling Flag
    this.wShortCastle = false;                // White Short Castling Flag
    this.bLongCastle = false;                 // Black Long Castling Flag
    this.bShortCastle = false;                // Black Short Castling Flag
    this.qRookInPos = false;                  // Black Queen Rook in Castling Position
    this.kRookInPos = false;                  // Black King Rook in Castling Position
    this.QRookInPos = false;                  // White Queen Rook in Castling Position
    this.KRookInPos = false;                  // White King Rook in Castling Position
    this.kInPos = false;                      // Black King in Castling Position
    this.KInPos = false;                      // White King in Castling Position
    this.wMove = false;                       // White next move
    this.bMove = false;                       // Black next move
    this.syntaxErrors = [];                   // Fen Syntax Erros Array
    this.noSysntaxErr = true;                 // Any Fen Syntax Errors?
    this.consistenyErrors = [];               // Fen Consistency Erros Array
    this.noConsistencyErr = true;             // Any Fen Conmsistency Errors?
    this.last_ValidPosition = '';             // Last Valid Pos in fen
    this.last_ValidCastlingRights = '';       // Last Valid Castling rights in fen
    this.last_ValidNextMove = '';             // Last Valid next move color piece in fen
    this.last_ValidMoveNumber = '';           // Last Valid move number in fen
    this.last_ValidEnPassantSquare = '';      // Last Valid en passant square in fen
    this.last_HalfMoveCounter = '';           // Last Valid half move counter in fen
    this.last_ValidFen = '';                  // Last Valid complete fen
    
    // Set model properties to fen and origin
    this.setModel = function (fenStr, origin) {
      var self = this;
      var posSyntaxErr = validate_fenSyntax(fenStr);        // Get syntax errors of new position
                                                            // Get consistency errors of new position if no syntax errors
                                                            // It also returns position flags object
                                                            // Update model properties
      self.syntaxErrors = posSyntaxErr;
      self.noSysntaxErr = (posSyntaxErr.length == 0);
      self.fen = fenStr;
      self.origin = origin;
      
      if (posSyntaxErr.length == 0) {
        var consistencyResults = validate_fenConsistency((posSyntaxErr.length == 0), fenStr);
        var posConsystencyErr = consistencyResults[0];
        var posFlags = consistencyResults[1][0];   

        self.wLongCastle = posFlags.wLongCastle;
        self.wShortCastle = posFlags.wShortCastle;
        self.bLongCastle = posFlags.bLongCastle;
        self.bShortCastle = posFlags.bShortCastle;
        self.qRookInPos = posFlags.qRookInPos;
        self.kRookInPos = posFlags.kRookInPos;
        self.QRookInPos = posFlags.QRookInPos;
        self.KRookInPos = posFlags.KRookInPos;
        self.kInPos = posFlags.kInPos;
        self.KInPos = posFlags.KInPos;
        self.wMove = posFlags.wMove;
        self.bMove = posFlags.bMove;

        self.consistenyErrors = posConsystencyErr;  
        self.noConsistencyErr = (posConsystencyErr.length == 0);
        var tokens = fenStr.split(/\s+/);
        self.last_ValidPosition = tokens[0];
        if (origin == 'user') {
          self.last_ValidNextMove = tokens[1];    
          self.last_ValidCastlingRights = tokens[2];
          self.last_ValidEnPassantSquare = tokens[3];
          self.last_HalfMoveCounter = tokens[4];
          self.last_ValidMoveNumber = tokens[5];
          self.last_ValidFen = fenStr;
        }
        else {
          self.last_ValidFen = self.last_ValidPosition + " " +
                               self.last_ValidNextMove + " " + 
                               self.last_ValidCastlingRights + " " +
                               self.last_ValidEnPassantSquare + " " +
                               self.last_HalfMoveCounter + " " +
                               self.last_ValidMoveNumber;
        }
      }
    }
    
};

// Instance of model, with empty board position. Change model origin to accept position changes from board.
model = new chessmodel();
model.setModel('8/8/8/8/8/8/8/8 w - - 0 1', 'user');
model.origin = 'board';