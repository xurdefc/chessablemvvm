// Validate Fen Consistency, uses chess.js to detect if kings are under check.
// Some error msgs include markup text to identify error element in fen
function validate_fenConsistency(noSyntaxErr, fen) {
  var err = [];
  var flags = [];
  var result = [];
  var error_msgs = [
    '',
    'No white king on the board.',
    'No black king on the board.',
    'white kings on the board.',
    'black kings on the board.',
    'Invalid short castling rights for white pieces.',
    'Invalid long castling rights for white pieces.',      
    'Invalid short castling rights for black pieces.',
    'Invalid long castling rights for black pieces.', 
    'Invalid next move options, white king under check.',
    'Invalid next move options, black king under check.',
    'Invalid pawn location at ranks 1 or 8.',
    'No white pieces on the board.',
    'No black pieces on the board.'
  ];

  if (!noSyntaxErr) {
    result.push(err);
    result.push(flags);
    return result;
    }
  
  var tokens = fen.split(/\s+/);
  
  // 1st and 3rd check: Number of white kings on board
  var nKings = tokens[0].split('K').length-1;
  if (nKings == 0) {
    err.push({error_number: 1, error: error_msgs[1]});
  }
  if (nKings > 1) {    
    err.push({error_number: 3, error: nKings.toString() + " " + error_msgs[3]});
  }
  
  // 2nd and 4th check: Number of black kings on board
  var nkings = tokens[0].split('k').length-1;
  if (nkings == 0) {
    err.push({error_number: 2, error: error_msgs[2]});
  }
  if (nkings > 1) {    
    err.push({error_number: 4, error: nkings.toString() + " " + error_msgs[4]});
  }

  var boardRows = tokens[0].split('/');

  //Update Global variables for castling rights checking
  var wLongCastle = (tokens[2].split('Q').length > 1);
  var wShortCastle = (tokens[2].split('K').length > 1);
  var bLongCastle = (tokens[2].split('q').length > 1);
  var bShortCastle = (tokens[2].split('k').length > 1);
  
  // Update Global variables for rooks position checking
  var qRookInPos = (boardRows[0].slice(0,1) == 'r');
  var kRookInPos = (boardRows[0].slice(-1) == 'r');
  var QRookInPos = (boardRows[7].slice(0,1) == 'R');
  var KRookInPos = (boardRows[7].slice(-1) == 'R');

  // Check if black king is in castling position and update global variable
  var kSquares = boardRows[0].split('k')
  var sum_squares = 0;
  for (var i = 0; i < kSquares[0].length; i++) {
    if (isNaN(kSquares[0][i])) { sum_squares++; }
    else { sum_squares+=parseInt(kSquares[0][i]); }
  }
  var kInPos = (sum_squares == 4);      
  
  // Check if white king is in castling position and update global variable
  var KSquares = boardRows[7].split('K')
  var sum_squares = 0;
  for (var i = 0; i < KSquares[0].length; i++) {
    if (isNaN(KSquares[0][i])) { sum_squares++; }
    else { sum_squares+=parseInt(KSquares[0][i]); }
  }
  var KInPos = (sum_squares == 4);   
  
  // 5th check: Correct placement of pieces for white short castling
  if (wShortCastle && !(KRookInPos && KInPos)) { err.push({error_number: 5, error: error_msgs[5]}); }
  
  // 6th check: Correct placement of pieces for white long castling
  if (wLongCastle && !(QRookInPos && KInPos)) { err.push({error_number: 6, error: error_msgs[6]}); }

  // 7th check: Correct placement of pieces for black short castling
  if (bShortCastle && !(kRookInPos && kInPos)) { err.push({error_number: 7, error: error_msgs[7]}); }
  
  // 8th check: Correct placement of pieces for black long castling
  if (bLongCastle && !(qRookInPos && kInPos)) { err.push({error_number: 8, error: error_msgs[8]}); }

  var bMove = true;
  
  // 9th check: Next move for black with white king under check    
  if ((nKings == 1) && (tokens[1] == 'b')) {
    var chess = new Chess(tokens[0] + ' w ' + tokens[2] + ' ' + tokens[3] + ' ' + tokens[4] + ' ' + tokens[5]);
    if (chess.in_check()) { 
      err.push({error_number: 9, error: error_msgs[9]});
      bMove = false;
    }
  }
  
  var wMove = true;
  
  // 10th check: Next move for white with black king under check 
   if ((nkings == 1) && (tokens[1] == 'w')) {
    var chess = new Chess(tokens[0] + ' b ' + tokens[2] + ' ' + tokens[3] + ' ' + tokens[4] + ' ' + tokens[5]);
    if (chess.in_check()) {
      err.push({error_number: 10, error: error_msgs[10]});
      wMove = false;
    }
  } 
  
  // 11th check: Pawn placements at ranks 1 or 8
  if ((boardRows[0].split('p').length > 1) || (boardRows[0].split('P').length > 1) ||
      (boardRows[7].split('p').length > 1) || (boardRows[7].split('P').length > 1)) {
        err.push({error_number: 11, error: error_msgs[11]});
  }

  // 12th check: Any white pieces on the board?
  if (!tokens[0].match('(P|R|N|B|Q|K)') ) {  
    err.push({error_number: 12, error: error_msgs[12]});
    wMove = false;
  }
  
  // 13th check: Any black pieces on the board?
  if (!tokens[0].match('(p|r|n|b|q|k)') ) { 
    err.push({error_number: 13, error: error_msgs[13]});
    bMove = false;
  }
  
  var flagsObj={};
  flagsObj.wLongCastle = wLongCastle;
  flagsObj.wShortCastle = wShortCastle;
  flagsObj.bLongCastle = bLongCastle;
  flagsObj.bShortCastle = bShortCastle;
  flagsObj.qRookInPos = qRookInPos;
  flagsObj.kRookInPos = kRookInPos;
  flagsObj.QRookInPos = QRookInPos;
  flagsObj.KRookInPos = KRookInPos;
  flagsObj.kInPos = kInPos;
  flagsObj.KInPos = KInPos;
  flagsObj.wMove = wMove;
  flagsObj.bMove = bMove;
  
  flags.push(flagsObj);
  
  result.push(err);
  result.push(flags);
  return result;    
}