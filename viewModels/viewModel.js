function chessView () {
  var self = this;
  
  // Set observable properties to update view if changed
  
  self.initialFenPos = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1';    
  self.emptyFenPos = '8/8/8/8/8/8/8/8 w - - 0 1';
  self.nextMoveIcon = ko.observable('img/panel-chesspieces/white-next-move-no-ok.png');
  self.whiteShortCastleIcon = ko.observable('img/panel-chesspieces/no-castling.png');
  self.whiteLongCastleIcon = ko.observable('img/panel-chesspieces/no-castling.png');
  self.blackShortCastleIcon = ko.observable('img/panel-chesspieces/no-castling.png');
  self.blackLongCastleIcon = ko.observable('img/panel-chesspieces/no-castling.png');
  self.consistencyStatus = ko.observable('fenConsistency fa fa-thumbs-down');
  self.syntaxStatus = ko.observable('fenSyntax fa fa-thumbs-up');
  self.fen = ko.observable('8/8/8/8/8/8/8/8 w - - 0 1');

  self.boardChange = function(oldPos, newPos) {
    if (model.origin != 'user') {                                       // Check if change is due to user typing 
      // Obtain new fen
      var newFen = ChessBoard.objToFen(newPos) +
                  ' ' + model.last_ValidNextMove +    
                  ' ' + model.last_ValidCastlingRights +
                  ' ' + model.last_ValidEnPassantSquare +
                  ' ' + model.last_HalfMoveCounter +
                  ' ' + model.last_ValidMoveNumber;
      model.setModel(newFen,'board');                                   // Update fen string and review errors, updating left panel if needed
      self.fen(newFen);
      self.updatePanel();
    }
  };
  
  self.typedFenChange = function () {                                   // Check FEN string by user typing 
    newFen = $('#fen').val();                                           // Update fen string and review errors, updating left panel if needed
    model.setModel(newFen,'user');
    self.updatePanel();
    if (model.noSysntaxErr) {                                           // Update pieces on board with new fen
      boardSetup.position(newFen, false);
    }
    model.origin ='board';
  };
  
  self.posClear = function () {                                         // Clear Board
    self.fen(self.emptyFenPos);
    self.typedFenChange();
  }

  self.posStart = function () {                                         // Setuo Initial Postion
    self.fen(self.initialFenPos);
    self.typedFenChange();   
  }

  self.posUndo = function () {                                          // Restore Last Valid Postion 
    self.fen(model.last_ValidFen);   
    self.typedFenChange();
  }
  
  self.updatePanel = function () {                                        // Update Left Panel Icons with new fen

    self.syntaxStatus(model.noSysntaxErr ? 'fenSyntax fa fa-thumbs-up' : 'fenSyntax fa fa-thumbs-down');
    if (model.noSysntaxErr) {
      self.consistencyStatus(model.noConsistencyErr ? 'fenConsistency fa fa-thumbs-up' : 'fenConsistency fa fa-thumbs-down');
    }
    else {
      self.consistencyStatus('fenConsistency fa fa-question');
    }
    self.updatePanelIcons();
  };
  
  // Set icons for new Fen 
  self.updatePanelIcons = function () {
    self.renderNextMoveColorPiece();
    self.renderWhiteShortCastling();
    self.renderWhiteLongCastling();
    self.renderBlackShortCastling();
    self.renderBlackLongCastling();
  };
  
  self.renderNextMoveColorPiece = function() {
    
    // Clear Icon if syntax error
    if (!model.noSysntaxErr) {
      self.nextMoveIcon('img/panel-chesspieces/no-next-move.png');
      return;
    } 
    
    // Decompose Fen
    var fen = $('#fen').val();
    var tokens = fen.split(/\s+/);
    
    // Assign Icons considering color and consistency of Fen
    switch (tokens[1]) {      
      case 'w':
        self.nextMoveIcon(model.wMove ? 'img/panel-chesspieces/white-next-move-ok.png' : 'img/panel-chesspieces/white-next-move-no-ok.png');
      break;
        
      case 'b':
        self.nextMoveIcon(model.bMove ? 'img/panel-chesspieces/black-next-move-ok.png' : 'img/panel-chesspieces/black-next-move-no-ok.png');
      break;
        
      default:
        self.nextMoveIcon('img/panel-chesspieces/no-next-move.png');
      break;      
    } 
    
  };

  self.renderWhiteShortCastling = function() {

    // Clear Icon if syntax error  
    if (!model.noSysntaxErr) {
      self.whiteShortCastleIcon('img/panel-chesspieces/no-castling.png');
      return;
    } 

    // Assign Icons considering Castling Rights and consistency of Fen  
    if (model.wShortCastle) {
      self.whiteShortCastleIcon((model.KRookInPos && model.KInPos) ? 'img/panel-chesspieces/white-short-castling-ok.png' : 'img/panel-chesspieces/white-short-castling-no-ok.png');
    }
    else {
      self.whiteShortCastleIcon('img/panel-chesspieces/no-castling.png');
    }  
  };
  
  self.renderWhiteLongCastling = function() {

    // Clear Icon if syntax error  
    if (!model.noSysntaxErr) {
      self.whiteLongCastleIcon('img/panel-chesspieces/no-castling.png');
      return;
    } 

    // Assign Icons considering Castling Rights and consistency of Fen  
    if (model.wLongCastle) {
      self.whiteLongCastleIcon((model.QRookInPos && model.KInPos) ? 'img/panel-chesspieces/white-long-castling-ok.png' : 'img/panel-chesspieces/white-long-castling-no-ok.png');
    }
    else {
      self.whiteLongCastleIcon('img/panel-chesspieces/no-castling.png');
    }  
  };
  
   self.renderBlackShortCastling = function() {

    // Clear Icon if syntax error  
    if (!model.noSysntaxErr) {
      self.blackShortCastleIcon('img/panel-chesspieces/no-castling.png');
      return;
    } 

    // Assign Icons considering Castling Rights and consistency of Fen  
    if (model.bShortCastle) {
      self.blackShortCastleIcon((model.kRookInPos && model.kInPos) ? 'img/panel-chesspieces/black-short-castling-ok.png' : 'img/panel-chesspieces/black-short-castling-no-ok.png');
    }
    else {
      self.blackShortCastleIcon('img/panel-chesspieces/no-castling.png');
    }  
  };
  
  self.renderBlackLongCastling = function() {

    // Clear Icon if syntax error  
    if (!model.noSysntaxErr) {
      self.blackLongCastleIcon('img/panel-chesspieces/no-castling.png');
      return;
    } 

    // Assign Icons considering Castling Rights and consistency of Fen  
    if (model.bLongCastle) {
      self.blackLongCastleIcon((model.qRookInPos && model.kInPos) ? 'img/panel-chesspieces/black-long-castling-ok.png' : 'img/panel-chesspieces/black-long-castling-no-ok.png');
    }
    else {
      self.blackLongCastleIcon('img/panel-chesspieces/no-castling.png');
    }  
  };
 
  // Allow user to change next move color piece by clicking
  self.toggleNextMoveColorPiece = function() { 
    if (model.noSysntaxErr) {
      var tokens = model.fen.split(/\s+/);

      // Change color in Fen
      if (self.nextMoveIcon().includes('white')) { tokens[1] = 'b'; }
      else if (self.nextMoveIcon().includes('black')) { tokens[1] = 'w'; }
      else { tokens[1] = 'w'; }

      var newFen = tokens.join(" ");
      self.fen(newFen);
      
      model.setModel(newFen,'user');
      
      // Update fen error icons, update panel icons
      self.updatePanel();
      model.origin ='board';
    }
  };
  
   // Allow user to change white short castling rights by clicking
  self.toggleWhiteShortCastling = function() {
    if (model.noSysntaxErr) {
      var tokens = model.fen.split(/\s+/);

      // Change white short castling rights
      if (self.whiteShortCastleIcon().includes('no-castling')) {
        tokens[2] = (tokens[2].replace('-','')+'K').split('').sort().join('');
      }
      else { 
        tokens[2] = tokens[2].replace('K','');
        if (tokens[2] == '') {tokens[2] = '-';}
      }
      var newFen = tokens.join(" ");
      self.fen(newFen);
      model.setModel(newFen,'user');
      
      // Update fen error icons, update panel icons
      self.updatePanel();
      model.origin ='board';
    }
  };

   // Allow user to change white long castling rights by clicking
  self.toggleWhiteLongCastling = function() {
    if (model.noSysntaxErr) {
      var tokens = model.fen.split(/\s+/);

      // Change white long castling rights
      if (self.whiteLongCastleIcon().includes('no-castling')) {
        tokens[2] = (tokens[2].replace('-','')+'Q').split('').sort().join('');
      }
      else { 
        tokens[2] = tokens[2].replace('Q','');
        if (tokens[2] == '') {tokens[2] = '-';}
      }
      var newFen = tokens.join(" ");
      self.fen(newFen);
      model.setModel(newFen,'user');
      
      // Update fen error icons, update panel icons
      self.updatePanel();
      model.origin ='board';
    }
  };
  
   // Allow user to change black short castling rights by clicking
  self.toggleBlackShortCastling = function() {
    if (model.noSysntaxErr) {
      var tokens = model.fen.split(/\s+/);

      // Change black short castling rights
      if (self.blackShortCastleIcon().includes('no-castling')) {
        tokens[2] = (tokens[2].replace('-','')+'k').split('').sort().join('');
      }
      else { 
        tokens[2] = tokens[2].replace('k','');
        if (tokens[2] == '') {tokens[2] = '-';}
      }
      var newFen = tokens.join(" ");
      self.fen(newFen);
      model.setModel(newFen,'user');
      
      // Update fen error icons, update panel icons
      self.updatePanel();
      model.origin ='board';
    }
  };

   // Allow user to change black long castling rights by clicking
  self.toggleBlackLongCastling = function() {
    if (model.noSysntaxErr) {
      var tokens = model.fen.split(/\s+/);

      // Change black long castling rights
      if (self.blackLongCastleIcon().includes('no-castling')) {
        tokens[2] = (tokens[2].replace('-','')+'q').split('').sort().join('');
      }
      else { 
        tokens[2] = tokens[2].replace('q','');
        if (tokens[2] == '') {tokens[2] = '-';}
      }
      var newFen = tokens.join(" ");
      self.fen(newFen);
      model.setModel(newFen,'user');
      
      // Update fen error icons, update panel icons
      self.updatePanel();
      model.origin ='board';
    }
  };

  // Create html content string with consistency errors for popover
  self.listConsistencyErrors = function() {
    var html="";
    if (!model.noConsistencyErr) {
      html+="<ul class='fa-ul'>\n";
      model.consistenyErrors.forEach(function(err) {
        html+="<li><i class='fa-li fa fa-exclamation-circle error-style'></i>" + err.error + "</li>\n";
      });
      html+="</ul>\n";
    }
    return html; 
  };

  // Create html content string with consistency errors for popover  
  self.listSyntaxErrors = function() {
    var html="";
    if (!model.noSysntaxErr) {
      var html="<ul class='fa-ul'>\n";
      model.syntaxErrors.forEach(function(err) {
        html+="<li><i class='fa-li fa fa-exclamation-circle error-style'></i>" + err.error + "</li>\n";
      });
      html+="</ul>\n";
    }
    return html; 
  } ; 
  
}

viewModel = new chessView()
ko.applyBindings(viewModel);