// Complete missing View elements
$('.fenStr').width($('.board-container').width());
  
var boardSetup = ChessBoard('boardSetup', {                    // Chessboard Draw
    draggable: true,
    dropOffBoard: 'trash',
    sparePieces: true,
    onChange: viewModel.boardChange, 
    position: viewModel.emptyFenPos        
  }
);

$('span.fenSyntax').webuiPopover({                             // Fen syntax errors popover
  title: 'Syntax Errors',
  content: viewModel.listSyntaxErrors,
  placement: 'right',
  dismissible: true,
  html: true,
  cache: false,
  hideEmpty: true
});

$('span.fenConsistency').webuiPopover({                         // Fen consistency errors popover
  title: 'Consistency Errors',
  content: viewModel.listConsistencyErrors,
  placement: 'right',
  dismissible: true,
  html: true,
  cache: false,
  hideEmpty: true
});